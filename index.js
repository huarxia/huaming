/**
 * XadillaX created at 2016-02-22 10:49:43 With ♥
 *
 * Copyright (c) 2016 Souche.com, all rights
 * reserved.
 */
"use strict";

module.exports = require("./lib/hua");
module.exports.Tangshi = require("./lib/hua_tangshi");
